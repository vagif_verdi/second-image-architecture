# SecondImage Architecture

## Introduction

This document is intended to describe some aspects of the architecture of the
SecondImage code bases.

## General

The following table gives a brief overview of each of the Haskell projects.

|Project Name|Description|
|------------|-----------|
|[bpdf-server](https://bitbucket.org/keaisdev/bpdf-server)|A service that aggregates individual PDF pages that have already been charted into a single PDF document.|
|[burn-cd-server](https://bitbucket.org/keaisdev/burn-cd-server)|CReating CD images with record files for burn cd equipment|
|[cust-chart-server](https://bitbucket.org/keaisdev/cust-chart-server)|creating records that were custom charted by customers|
|[db-service](https://bitbucket.org/keaisdev/db-service)|daily update of various tables in sql server, examples are deleting old records or computing some values etc|
|[dicom-haskell-library](https://bitbucket.org/keaisdev/dicom-haskell-library)|A library for reading DICOM image files. This is a fork of [a library on Hackage](https://hackage.haskell.org/package/dicom).|
|[edt-server](https://bitbucket.org/keaisdev/edt-server)| electronic transfer of records to some of our customers|
|[email-gateway](https://bitbucket.org/keaisdev/email-gateway)|a gateway for all our services to send email via http API in order to avoid setting every vm with postfix relay|
|[email-notif](https://bitbucket.org/keaisdev/email-notif)|Shipping email notifications|
|[ephesoft](https://bitbucket.org/keaisdev/ephesoft)|parsing outout of ephesoft software|
|[hdbc-odbc](https://bitbucket.org/keaisdev/hdbc-odbc)|A library for interfacing with databases in Haskell. This is an old fork of [a library on Hackage](https://hackage.haskell.org/package/HDBC-odbc).|
|[hs-util](https://bitbucket.org/keaisdev/hs-util)|Library that contains generally useful funtionality used by some of the other projects.|
|[hxray](https://bitbucket.org/keaisdev/hxray)|parsing dicom files and extracting jepgs out of them for online view|
|[keais-import](https://bitbucket.org/keaisdev/keais-import)|importing workorders from keais system|
|[ocr-server](https://bitbucket.org/keaisdev/ocr-server)|A service that performs optical character recoginition (OCR) on individual PDF pages.|
|[pdf-server](https://bitbucket.org/keaisdev/pdf-server)|server that provides various pdf artifacts like cover page or Table of Contents to Sirus |
|[record-toc](https://bitbucket.org/keaisdev/record-toc)|Creates a table of contents (TOC) from the "records" PDF.|
|[si-pdflib](https://bitbucket.org/keaisdev/si-pdflib)|library to work with pdf via FFI to pdflib commercial library|
|[si-pdflib-templates](https://bitbucket.org/keaisdev/si-pdf-templates)|common pdf building code like coverpages etc, used by several other applications|
|[sirus2](https://bitbucket.org/keaisdev/sirus2)|web UI for Sirus, for example various task queues, OPAQ, charting etc|
|[sirus-server](https://bitbucket.org/keaisdev/sirus-server)|JSON API server for Sirus|
|[si-sso](https://bitbucket.org/keaisdev/si-sso)|Single SIgn On for Secondimage web site|
|[si-web](https://bitbucket.org/keaisdev/si-web)|Customizer and some other web modules for SecondImage web site|
|[smartchron-server](https://bitbucket.org/keaisdev/smartchron-server)|creating smartchron records for keais|
|[split-pdf-server](https://bitbucket.org/keaisdev/split-pdf-server)|splitting large pdfs based on customer properties into smaller volumes|
|[tp-chart](https://bitbucket.org/keaisdev/tp-chart)|Third Party Charting|

## Docker

Some, but not all, of the projects are built using Docker.  The following
diagram shows the relationship between each project's docker image with the
images on which they are based.

![Docker Images Dependencies](img/docker-builds.png)
