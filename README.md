# SecondImage Architecture

This repo is intended to be a source of information about the architecture of
the various applications from SecondImage.  See [the architecture document](ARCHITECTURE.md).

## Building

To build just run `make` in the project directory.
